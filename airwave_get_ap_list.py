#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Connect to Airwave REST API to get AP list"""


# Debian packages: python3-requests, python3-lxml
import xml.etree.ElementTree as ET # libxml2 and libxslt
import requests                    # HTTP requests
import csv
import sys

# ---------------------------------------------------------------------------
# Constants
# ---------------------------------------------------------------------------

# Login/password for Airwave (read-only account)
LOGIN = 'operator'
PASSWD = 'verylongpasswordforyourreadonlyaccount'

# URL for REST API
LOGIN_URL = 'https://aruba-airwave.example.com/LOGIN'
AP_LIST_URL = 'https://aruba-airwave.example.com/ap_list.xml'

# Delimiter for CSV output
DELIMITER = ';'

# HTTP headers for each HTTP request
HEADERS = {
    'Content-Type' : 'application/x-www-form-urlencoded',
    'Cache-Control' : 'no-cache'
}

# ---------------------------------------------------------------------------
# Fonctions
# ---------------------------------------------------------------------------

def open_session():
    """Open HTTPS session with login"""

    ampsession = requests.Session()
    data = 'credential_0={0}&credential_1={1}&destination=/&login=Log In'.format(LOGIN, PASSWD)
    loginamp = ampsession.post(LOGIN_URL, headers=HEADERS, data=data)
    return {'session' : ampsession, 'login' : loginamp}

def get_ap_list(session):
    """Get XML data and returns a dictionnaries list"""

    # Get XML (one long string)
    output = session.get(AP_LIST_URL, headers=HEADERS)
    ap_list_output = output.content

    # Parse XML for desired attributes and build a dictionnaries list
    tree = ET.fromstring(ap_list_output)
    ap_list = []
    for i in tree.iter(tag='ap'):
        fqdn = i.find('fqdn').text
        lan_ip = i.find('lan_ip').text
        if i.find('lan_mac') is not None:
            lan_mac = i.find('lan_mac').text
        else:
            lan_mac = "None"
        mfgr = i.find('mfgr').text
        model = i.find('model').text
        syslocation = i.find('syslocation').text

        ap_list.append({'lan_mac' : lan_mac,
                        'lan_ip' : lan_ip,
                        'fqdn' : fqdn,
                        'mfgr' : mfgr,
                        'model' : model,
                        'syslocation' : syslocation})
    return ap_list

def print_into_csv(ap_list):
    """Print a dictionnaries list into CSV format"""

    fieldnames = ['lan_mac', 'lan_ip', 'fqdn', 'mfgr', 'model', 'syslocation']
    writer = csv.DictWriter(sys.stdout, fieldnames=fieldnames, delimiter=DELIMITER)
    writer.writerows(ap_list)

def main():
    """Main function"""

    session = open_session()
    ap_list = get_ap_list(session['session'])
    print_into_csv(ap_list)

# ---------------------------------------------------------------------------
# Call main
# ---------------------------------------------------------------------------

main()
