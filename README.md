# aruba-airwave-rest

This project aims to host small Python 3 scripts as examples to use
Aruba Airwave's REST API.

The official paper documentation wasn't so helpful to me, but [this
video tutorial](https://www.youtube.com/watch?v=LcMSEFY9T_c) was.

## airwave_get_ap_list.py

This script calls the `ap_list.xml` method to retrieve the list of all
wifi AP known by your Airwave installation and returns it as a CSV output.

Python 3 dependencies on a Debian system:

    sudo apt install python3-requests python3-lxml

Hope this helps.
